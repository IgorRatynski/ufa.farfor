//
//  MainMenuFlowLayout.m
//  ufa.farfor
//
//  Created by Igor on 20.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "MainMenuFlowLayout.h"

@implementation MainMenuFlowLayout

- (instancetype)init {
    
    self = [super init];
    if (self) {
        
        self.minimumLineSpacing         = 1.0;
        self.minimumInteritemSpacing    = 1.0;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return self;
}


- (CGSize)itemSize
{
    NSInteger numberOfColumns = 2;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame) - (numberOfColumns - 1)) / numberOfColumns;
    return CGSizeMake(itemWidth, itemWidth);
}


@end
