//
//  FoodOfferController.m
//  ufa.farfor
//
//  Created by Igor on 27.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "FoodOfferController.h"
#import "Offer.h"
#import "OfferImageCell.h"
#import "OfferDescriptionCell.h"



@interface FoodOfferController ()


@end

@implementation FoodOfferController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        OfferImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OfferImageCell" forIndexPath:indexPath];
        
        [cell.offerImage sd_setImageWithURL:_offer.imageURL
                           placeholderImage:[UIImage imageNamed:@"PlaceHolder.jpg"]
                                    options:SDWebImageProgressiveDownload
                                   progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                   }
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      image = [image scaleToSize:[self proportionalImageSizeFrom:image.size]];
                                      cell.offerImage.image = image;
                                  }];
        
        return cell;
        
    } else if (indexPath.row == 1) {
        OfferDescriptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OfferDescriptionCell" forIndexPath:indexPath];
        
        cell.name.text = _offer.name;
        cell.weight.text = [NSString stringWithFormat:@"Вес %@.",[_offer.params valueForKey:@"Вес"]];
        cell.price.text = [NSString stringWithFormat:@"Цена: %1.2f рублей.", _offer.price];
        cell.offerDescription.text = _offer.descript;
        
        return cell;
    }
    
    return nil;
}

- (CGSize)proportionalImageSizeFrom:(CGSize)imageSize {
    
    CGFloat ratio  = [UIScreen mainScreen].bounds.size.width / imageSize.width;
    CGFloat heigth = imageSize.height * ratio;
    
    return CGSizeMake((NSUInteger)[UIScreen mainScreen].bounds.size.width,
                             (NSUInteger)heigth);
}


@end
