//
//  FoodOfferController.h
//  ufa.farfor
//
//  Created by Igor on 27.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Offer;

@interface FoodOfferController : UITableViewController

@property (nonatomic, strong) Offer* offer;

@end
