//
//  FoodMenuController.m
//  ufa.farfor
//
//  Created by Igor on 18.02.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//


#import "FoodMenuController.h"
#import "MainViewController.h"
#import "MainMenuItem.h"
#import "AppDelegate.h"
#import "ReactiveCocoa.h"
#import "Category.h"
#import "Offer.h"
#import "MainMenuFlowLayout.h"
#import "FoodCategoryController.h"
#import "API.h"
#import "MBProgressHUD.h"
#import "TMCache.h"

/* изменить инит _UICollectionViewController, чтобы загружалась только при обновлении данных */

@interface FoodMenuController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIButton *button;

@property (strong, nonatomic) NSArray<Category *>* categories;

@end

@implementation FoodMenuController

-  (void)viewDidLoad {
    
    _categories = [[API sharedAPI] getCategories];
    
    if (_categories == nil) {
        [APP showActivityIndicator:@"Loading..." currentView:self.view];
    }
    
     self.collectionView.collectionViewLayout = [[MainMenuFlowLayout alloc] init];
     self.collectionView.refreshControl = [UIRefreshControl new];
    [self.collectionView.refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    
    [self.collectionView setDataSource:self];
}

- (void)awakeFromNib {
    [super awakeFromNib];

    UIImage* menuImg = [UIImage imageNamed:@"menu-standart-128x128.png"];
    menuImg = [menuImg imageWithTintColor:[UIColor darkGrayColor]];
    menuImg = [menuImg scaleToSize:(CGSize){22, 22}];
    
    self.navigationItem.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithImage:menuImg
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(openLeftView)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshUI:)
                                                 name:kNotificationCategoriesWasUpdated
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(taskDone)
                                                 name:kNotificationDownloadedCategoriesIsEqualToCached
                                               object:nil];
    
    [RACObserve(self, self.categories) subscribeNext:^(NSArray <Category *> *newValue) {
        
        if (newValue != nil && newValue.count != 0) {
            
            _categories  = newValue;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.collectionView reloadData];
            });
            [APP hideActivityIndicator];
        }
    }];
}



- (void)refreshUI:(NSNotification *)notification {
    
    NSArray <Category*> *categories = [notification.userInfo valueForKey:kCategories];
    
    self.categories = categories;
    
    [self taskDone];
}

#pragma mark - refresh control

- (void)handleRefresh:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNeedUpdateData object:self];
}

-(void)taskDone {
    
    [self.collectionView.refreshControl endRefreshing];
}


#pragma mark -

- (void)openLeftView {
    
    [kMainViewController  showLeftViewAnimated:YES completionHandler:nil];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"FoodCategoryControllerSegue"]) {
        
        FoodCategoryController *controller = (FoodCategoryController *)[segue destinationViewController];
        
        NSUInteger index = self.collectionView.indexPathsForSelectedItems[0].row;
        [controller setCategoryID:_categories[index].identifier];
        
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
    
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.categories.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Offer* firstOffer = (Offer *)[[[API sharedAPI] getOffersWithCategoryID:_categories[indexPath.row].identifier] firstObject];
    
    MainMenuItem* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"item" forIndexPath:indexPath];
    
    [cell.backgroundView sd_setImageWithURL:firstOffer.imageURL
                           placeholderImage:[UIImage imageNamed:@"PlaceHolder.jpg"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                      
                                      image = [image scaleToSize:cell.bounds.size];
                                      cell.backgroundView.image = image;
                                  }];
    
    cell.nameLabel.text = _categories[indexPath.row].name;
    
    return cell;
}

@end
