//
//  LeftViewController.h
//  ufa.farfor
//
//  Created by Igor on 18.02.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuController : UITableViewController

@property (strong, nonatomic) UIColor *tintColor;

@end
