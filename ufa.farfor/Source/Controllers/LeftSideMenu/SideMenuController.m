//
//  LeftViewController.m
//  ufa.farfor
//
//  Created by Igor on 18.02.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "SideMenuController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "SideMenuCell.h"
#import "FoodMenuController.h"
#import "MapViewController.h"

@interface SideMenuController ()

@property (strong, nonatomic) NSArray *titlesArray;

@end

@implementation SideMenuController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _titlesArray = @[@"",
                     @"Меню",
                     @"Контакты"];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
}

#pragma mark -

- (void)openLeftView
{
    [kMainViewController showLeftViewAnimated:YES completionHandler:nil];
}

- (void)openRightView
{
    [kMainViewController showRightViewAnimated:YES completionHandler:nil];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _titlesArray.count;
}

#pragma mark - UITableView Delegate

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SideMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    cell.textLabel.text = _titlesArray[indexPath.row];
    
    cell.userInteractionEnabled = ![_titlesArray[indexPath.row] isEqualToString:@""];
    
    cell.tintColor = _tintColor;
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) return 50.f;
    else return 100.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        FoodMenuController * menuController = [self.storyboard instantiateViewControllerWithIdentifier:@"FoodMenuController"];
        
        [kNavigationController setViewControllers:@[menuController]];
    }
    if (indexPath.row == 2) {
        
        MapViewController * mapController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        
        [kNavigationController setViewControllers:@[mapController]];
    }
    
    [kMainViewController hideLeftViewAnimated:YES completionHandler:nil];
}

@end
