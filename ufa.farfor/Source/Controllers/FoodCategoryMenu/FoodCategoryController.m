//
//  FoodCategoryController.m
//  ufa.farfor
//
//  Created by Igor on 20.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "FoodCategoryController.h"
#import "Category.h"
#import "Offer.h"
#import "FoodOfferCell.h"

#import "API.h"

#import "FoodOfferController.h"

@interface FoodCategoryController() <UITableViewDataSource>

@property (nonatomic, strong) NSArray<Offer *>* offers;
@property (nonatomic) NSUInteger categoryID;

@end

@implementation FoodCategoryController


- (void)setCategoryID:(NSUInteger)ID {
    _categoryID = ID;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshUI:)
                                                 name:kNotificationOffersWasUpdated
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(taskDone)
                                                 name:kNotificationDownloadedOffersIsEqualToCached
                                               object:nil];
}

- (void)refreshUI:(NSNotification *)notification {
    
    [self taskDone];
}
- (void)viewDidLoad {
    self.tableView.refreshControl = [UIRefreshControl new];
    [self.tableView.refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    
    _offers = [[API sharedAPI] getOffersWithCategoryID:_categoryID];
    [self.tableView reloadData];
}

- (void)handleRefresh:(id)sender {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNeedUpdateData object:self];
}

-(void)taskDone {
    
    [self.tableView.refreshControl endRefreshing];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _offers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodOfferCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    [cell.offerImage sd_setImageWithURL:_offers[indexPath.row].imageURL
                       placeholderImage:[UIImage imageNamed:@"PlaceHolder.jpg"]
                                options:SDWebImageProgressiveDownload
                               progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    }
                              completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                  
    }];
    
    cell.offerName.text = _offers[indexPath.row].name;
    cell.offerPrice.text = [NSString stringWithFormat:@"%1.2f", _offers[indexPath.row].price];
    cell.offerDescription.text = [NSString stringWithFormat:@"Вес %@.", [_offers[indexPath.row].params valueForKey:@"Вес"]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"FoodOfferControllerSegue"]) {
        
        FoodOfferController *controller = (FoodOfferController *)[segue destinationViewController];
        
        [controller setOffer:_offers[self.tableView.indexPathForSelectedRow.row]];
        
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
    
}

@end
