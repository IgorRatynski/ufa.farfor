//
//  FoodCategoryController.h
//  ufa.farfor
//
//  Created by Igor on 20.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodCategoryController : UITableViewController

- (void)setCategoryID:(NSUInteger)ID;

@end
