//
//  MainViewController.m
//  ufa.farfor
//
//  Created by Igor on 24.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "MainViewController.h"
#import "FoodMenuController.h"
#import "SideMenuController.h"
#import "AppDelegate.h"

@interface MainViewController ()

@property (strong, nonatomic) SideMenuController  *  sideMenuController;

@end

@implementation MainViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.rootViewController  = [self.storyboard
                                instantiateViewControllerWithIdentifier:@"NavigationController"];

    _sideMenuController      = [self.storyboard
                                instantiateViewControllerWithIdentifier:@"SideMenuController"];
    
    
    [self setLeftViewEnabledWithWidth:100.f
                    presentationStyle:LGSideMenuPresentationStyleSlideBelow
                 alwaysVisibleOptions:0];
    
    self.leftViewBackgroundImage = [UIImage imageNamed:@"image"];
    
    _sideMenuController.tableView.backgroundColor = [UIColor clearColor];
    _sideMenuController.    tintColor = [UIColor whiteColor];
    [_sideMenuController.tableView reloadData];
    [self.leftView addSubview:_sideMenuController.tableView];
    
}

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size {
    
    [super leftViewWillLayoutSubviewsWithSize:size];
    _sideMenuController.tableView.frame = CGRectMake(0.f , 0.f, size.width, size.height);
}




@end
