//
//  MapViewController.m
//  ufa.farfor
//
//  Created by Igor on 14.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <MapKit/MKMapView.h>

#import "MapViewController.h"
#import "AppDelegate.h"
#import "MainViewController.h"
#import "MapView.h"
#import "ClientMapAnnotation.h"
#import "AppDelegate.h"

@interface MapViewController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MapView *map;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UILabel *address;

@end

@implementation MapViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [APP showActivityIndicator:@"Loading..." currentView:self.map];
    [self setup];
    
    UIImage* menuImg = [UIImage imageNamed:@"menu-standart-128x128.png"];
    
    menuImg = [menuImg imageWithTintColor:[UIColor darkGrayColor]];
    menuImg = [menuImg scaleToSize:(CGSize){22, 22}];
    
    self.navigationItem.leftBarButtonItem  = [[UIBarButtonItem alloc] initWithImage:menuImg
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(openLeftView)];
}

- (void) setup {
    [self.map setDelegate:self];
}

- (void)openLeftView
{
    [kMainViewController  showLeftViewAnimated:YES completionHandler:nil];
}

#pragma mark - MKMapViewDelegate


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    id annotation = view.annotation;
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        return;
    }
    
    if([annotation conformsToProtocol:@protocol(MKAnnotation)]) {
        
        ClientMapAnnotation* annotation = view.annotation;
        
        _phone.text    = [NSString stringWithFormat:@"Телефон: +%@", annotation.phone];
        _address.text  = [NSString stringWithFormat:@"%@", annotation.subtitle];
    }
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    
    const CGFloat delta = 0.055;
    
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta  = delta;
    span.longitudeDelta = delta;
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.span = span;
    region.center = location;
    [aMapView setRegion:region animated:YES];
}
- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered {
    [APP hideActivityIndicator];
}
@end
