//
//  Client.m
//  ufa.farfor
//
//  Created by Igor on 03.12.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "Cafe.h"


@implementation CafeList

static const int cafeCount = 10;

static NSString*  cafeNames[] = {
    @"Grand Bellagio",
    @"Контрабас",
    @"Яблоко",
    @"ВиноГрадъ",
    @"Эриван",
    @"Wood & Fire",
    @"Piaffer",
    @"Dругое место",
    @"Гагарин",
    @"Cуши Весла"
};
static NSString*  cafeAdresses[] = {
    @"г. Минск, пр-т Машерова, 17",
    @"г. Минск, ул. Революционная, 10",
    @"г. Минск, ул. Л. Карастояновой, 32",
    @"г. Минск, ул. Лобанка, 77",
    @"г. Минск, 1-й Загородный пер., 3",
    @"г. Минск, ул. Ф. Скорины, 1",
    @"г. Минск, ул. Одоевского, 117",
    @"г. Минск, ул. Сурганова, 58",
    @"г. Минск, пр-т Партизанский, 2/1",
    @"г. Минск, ул. П. Глебки, 5"
};


+ (NSArray<Cafe*>*) getCafeList {
    NSMutableArray<Cafe*> * tempArray = [NSMutableArray new];
    
    for (int i = 0; i < cafeCount; i++) {
        Cafe* cafe = [Cafe cafeWithName:cafeNames[i] andAddress:cafeAdresses[i]];
        [tempArray addObject:cafe];
    }
    
    return tempArray;
}

@end



@implementation Cafe


+  (Cafe*) cafeWithName:(NSString*)name andAddress:(NSString*)address {
    Cafe* cafe = [Cafe new];
     if (cafe) {
        [cafe setName:name];
        [cafe setAddress:address];
        [cafe setPhone:[cafe randomPhone]];
        [cafe setCoordinate:[cafe geoCodeUsingAddress:address]];
    }
return cafe;
}
- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    } else {
        NSLog(@"Address, %@ not found",address);
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}
- (NSString*)randomPhone {
    NSMutableString* phoneNumber = [NSMutableString stringWithFormat:@"375"];
    
    while (phoneNumber.length < 12) {
        NSUInteger character = arc4random_uniform(10);
        [phoneNumber appendString:[NSString stringWithFormat:@"%d", character]];
    }
    
    return phoneNumber;
}

@end
