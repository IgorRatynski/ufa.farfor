//
//  Client.h
//  ufa.farfor
//
//  Created by Igor on 03.12.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Cafe;

@interface CafeList : NSObject

+ (NSArray<Cafe*>*) getCafeList;

@end

@interface Cafe : NSObject

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSString* phone;
@property (strong, nonatomic) NSString* address;
@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

+ (Cafe*) cafeWithName:(NSString*)name andAddress:(NSString*)address;
- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address;

@end
