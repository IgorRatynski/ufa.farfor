//
//  FoodOffer.m
//  ufa.farfor
//
//  Created by Igor on 17.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "Offer.h"

#define OFFER_ID            @"offerID"
#define OFFER_CATEGORY_ID   @"offerCategoryID"
#define OFFER_PRICE         @"offerPrice"
#define OFFER_NAME          @"offerName"
#define OFFER_DESCRIPTION   @"offerDescription"
#define OFFER_SHOP_URL      @"offerShopURL"
#define OFFER_IMAGE_URL     @"offerImageURL"
#define OFFER_PARAMS        @"offerParams"

@interface Offer()

@property (nonatomic, readwrite, assign) NSUInteger identifier;
@property (nonatomic, readwrite, assign) NSUInteger categoryId;

@property (nonatomic, readwrite, assign) double price;

@property (nonatomic, readwrite, strong) NSString *name;
@property (nonatomic, readwrite, strong) NSString *descript;

@property (nonatomic, readwrite, strong) NSURL *shopURL;
@property (nonatomic, readwrite, strong) NSURL *imageURL;

@property (nonatomic, readwrite, strong) NSDictionary *params;

@end

@implementation Offer

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.identifier   forKey:OFFER_ID];
    [aCoder encodeInteger:self.categoryId   forKey:OFFER_CATEGORY_ID];
    [aCoder encodeDouble:self.price         forKey:OFFER_PRICE];
    [aCoder encodeObject:self.name          forKey:OFFER_NAME];
    [aCoder encodeObject:self.descript      forKey:OFFER_DESCRIPTION];
    [aCoder encodeObject:self.shopURL       forKey:OFFER_SHOP_URL];
    [aCoder encodeObject:self.imageURL      forKey:OFFER_IMAGE_URL];
    [aCoder encodeObject:self.params        forKey:OFFER_PARAMS];
}
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    
    if (self) {
        self.identifier  = [aDecoder decodeIntegerForKey:OFFER_ID];
        self.categoryId  = [aDecoder decodeIntegerForKey:OFFER_CATEGORY_ID];
        self.price       = [aDecoder  decodeDoubleForKey:OFFER_PRICE];
        self.name        = [aDecoder  decodeObjectForKey:OFFER_NAME];
        self.descript    = [aDecoder  decodeObjectForKey:OFFER_DESCRIPTION];
        self.shopURL     = [aDecoder  decodeObjectForKey:OFFER_SHOP_URL];
        self.imageURL    = [aDecoder  decodeObjectForKey:OFFER_IMAGE_URL];
        self.params      = [aDecoder  decodeObjectForKey:OFFER_PARAMS];
    }
    
    return self;
}

#pragma mark - setters

- (void)setIdentifier:(NSUInteger)identifier {
    if (_identifier != identifier) {
        _identifier  = identifier;
    }
}

- (void)setCategoryId:(NSUInteger)categoryId {
    if (_categoryId != categoryId) {
        _categoryId  = categoryId;
    }
}

- (void)setPrice:(double)price {
    if (_price != price) {
        _price  = price;
    }
}

- (void)setName:(NSString *)name {
    if (![_name isEqualToString:name]) {
          _name = name;
    }
}

- (void)setDescript:(NSString *)descript {
    if (![_descript isEqualToString:descript]) {
          _descript = descript;
    }
}

- (void)setShopURL:(NSURL *)shopURL {
    if (![_shopURL isEqual:shopURL]) {
          _shopURL = shopURL;
    }
}

- (void)setImageURL:(NSURL *)imageURL {
    if (![_imageURL isEqual:imageURL]) {
          _imageURL = imageURL;
    }
}

- (void)setParams:(NSDictionary *)params {
    if (![_params isEqual:params]) {
        _params = params;
    }
}

@end
