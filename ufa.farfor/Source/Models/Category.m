//
//  FoodCategory.m
//  ufa.farfor
//
//  Created by Igor on 17.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "Category.h"

#define CATEGORY_ID         @"categoryID"
#define CATEGORY_NAME       @"categoryName"

@interface Category()

@property (nonatomic, readwrite, assign) NSUInteger identifier;
@property (nonatomic, readwrite, copy)   NSString *name;

@end

@implementation Category

- (instancetype)initWithCategoryData:(CategoryData)categoryData {
    self = [super init];
    if (self) {
        _identifier = categoryData.identifier;
        _name = categoryData.name;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeInteger:self.identifier  forKey:CATEGORY_ID];
    [aCoder encodeObject:self.name         forKey:CATEGORY_NAME];
}
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super init];
    
    if (self) {
        self.identifier  =  [aDecoder decodeIntegerForKey:CATEGORY_ID];
        self.name        =  [aDecoder decodeObjectForKey:CATEGORY_NAME];
    }
    
    return self;
}

@end
