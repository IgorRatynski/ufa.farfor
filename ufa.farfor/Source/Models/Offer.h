//
//  FoodOffer.h
//  ufa.farfor
//
//  Created by Igor on 17.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Offer : NSObject <NSCoding>


@property (nonatomic, readonly, assign) NSUInteger identifier;
@property (nonatomic, readonly, assign) NSUInteger categoryId;

@property (nonatomic, readonly, assign) double price;

@property (nonatomic, readonly, strong) NSString *name;
@property (nonatomic, readonly, strong) NSString *descript;

@property (nonatomic, readonly, strong) NSURL *shopURL;
@property (nonatomic, readonly, strong) NSURL *imageURL;

@property (nonatomic, readonly, strong) NSDictionary *params;


- (void)setIdentifier:(NSUInteger)identifier;
- (void)setCategoryId:(NSUInteger)categoryId;
- (void)setPrice:(double)price;
- (void)setName:(NSString *)name;
- (void)setDescript:(NSString *)descript;
- (void)setShopURL:(NSURL *)shopURL;
- (void)setImageURL:(NSURL *)imageURL;
- (void)setParams:(NSDictionary *)params;


@end
