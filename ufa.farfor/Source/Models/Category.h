//
//  Category.h
//  ufa.farfor
//
//  Created by Igor on 17.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    NSUInteger identifier;
    __unsafe_unretained NSString * name;
} CategoryData;

@interface Category : NSObject <NSCoding>

@property (nonatomic, readonly, assign) NSUInteger identifier;
@property (nonatomic, readonly, copy)   NSString *name;

- (instancetype)initWithCategoryData:(CategoryData)categoryData;

@end
