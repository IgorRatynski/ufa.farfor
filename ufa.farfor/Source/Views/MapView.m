//
//  MapVC.m
//  ufa.farfor
//
//  Created by Igor on 03.12.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <MapKit/MKAnnotation.h>
#import "MapView.h"
#import "Cafe.h"
#import "ClientMapAnnotation.h"
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotationView.h>
#import <CoreLocation/CLLocationManager.h>

@interface MapView () <CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager* locationManager;
@property (strong, nonatomic) CLLocation* location;

@end

@implementation MapView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setup];
}

- (void)viewDidLoad {
    [self setup];
}

- (void)setup {
    
     self.locationManager = [CLLocationManager new];
    
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];
    
    [self addCafeInMap];
    
    self.showsUserLocation = YES;
    self.showsBuildings    = YES;
    
    [self setMapType:MKMapTypeHybrid];
}

- (void)addCafeInMap {
    for (Cafe* cafe in [CafeList getCafeList]) {
        [self addAnnotation:[ClientMapAnnotation initWithCafe:cafe]];
    }
    [self actionShowAll];
}

-(void)actionShowAll {
static double delta = 20000.f;
    
    MKMapRect zoomRect = MKMapRectNull;
    
    for (id<MKAnnotation> annotation in self.annotations) {
        
        MKMapPoint center = MKMapPointForCoordinate(annotation.coordinate);
        
        MKMapRect rect = MKMapRectMake(center.x - delta, center.y - delta, 2 * delta, 2 * delta);
        
        zoomRect = MKMapRectUnion(zoomRect, rect);
    }
    
    zoomRect = [self mapRectThatFits:zoomRect];
    
    [self setVisibleMapRect:zoomRect
                        edgePadding:UIEdgeInsetsMake(50, 50, 50, 50)
                           animated:YES];
}

@end
