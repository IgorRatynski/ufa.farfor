//
//  FoodOfferCell.h
//  ufa.farfor
//
//  Created by Igor on 21.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodOfferCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *offerImage;
@property (weak, nonatomic) IBOutlet UILabel *offerName;
@property (weak, nonatomic) IBOutlet UILabel *offerDescription;
@property (weak, nonatomic) IBOutlet UILabel *offerPrice;

@end
