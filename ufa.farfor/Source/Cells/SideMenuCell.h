//
//  SideMenuCell.h
//  ufa.farfor
//
//  Created by Igor on 26.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuCell : UITableViewCell

@property (strong, nonatomic) UIView *separatorView;
@property (strong, nonatomic) UIColor *tintColor;

@end
