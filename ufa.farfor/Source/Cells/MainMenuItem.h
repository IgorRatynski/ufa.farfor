//
//  MainMenuItem.h
//  ufa.farfor
//
//  Created by Igor on 04.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuItem : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
