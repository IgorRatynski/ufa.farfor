//
//  OfferImageCell.h
//  ufa.farfor
//
//  Created by Igor on 27.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferImageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *offerImage;


@end
