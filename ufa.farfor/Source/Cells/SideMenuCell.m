//
//  SideMenuCell.m
//  ufa.farfor
//
//  Created by Igor on 26.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "SideMenuCell.h"

@interface SideMenuCell ()

@end

@implementation SideMenuCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.backgroundColor = [UIColor clearColor];
    
    self.textLabel.font = [UIFont systemFontOfSize:15.f];
    
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.numberOfLines = 0;
    self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    // -----
    
    _separatorView = [UIView new];
    [self addSubview:_separatorView];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.textLabel.textColor = _tintColor;
    _separatorView.backgroundColor = [_tintColor colorWithAlphaComponent:0.4];
    
    CGFloat height = ([UIScreen mainScreen].scale == 1.f ? 1.f : 0.5);
    
    _separatorView.frame = CGRectMake(0.f, self.frame.size.height-height, self.frame.size.width*0.9, height);
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted)
        self.textLabel.textColor = [UIColor colorWithRed:0.f green:0.5 blue:1.f alpha:1.f];
    else
        self.textLabel.textColor = _tintColor;
}

@end
