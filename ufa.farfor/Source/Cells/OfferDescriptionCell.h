//
//  OfferDescriptionCell.h
//  ufa.farfor
//
//  Created by Igor on 27.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferDescriptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *weight;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *offerDescription;

@end
