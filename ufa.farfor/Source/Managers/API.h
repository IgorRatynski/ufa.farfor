//
//  API.h
//  ufa.farfor
//
//  Created by Igor on 02.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

/*
 знаю что комментарии не желательны, но думаю что следует сказать что класс создан на случай если нужно будет работать с каким-нибудь другим классом, а не только с загрузчиком
 */
#import <Foundation/Foundation.h>

@class Category;
@class Offer;
@class Cafe;

@interface API : NSObject

+ (nonnull API *)sharedAPI;

- (void)downloadData;

- (nonnull NSArray<Category *> *)getCategories;
- (nonnull NSArray<Offer *> *)getOffersWithCategoryID:(NSUInteger)ID;
- (nonnull NSArray<Cafe *>*)getCafeList;

@end
