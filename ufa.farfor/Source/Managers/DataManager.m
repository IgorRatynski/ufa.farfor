//
//  DataManager.m
//  ufa.farfor
//
//  Created by Igor on 03.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import "DataManager.h"
#import "TMCache.h"
#import "Parser.h"
#import "Category.h"
#import "Offer.h"
#import "Cafe.h"


@interface DataManager()

@property (nonatomic, strong) Parser        *   parser;


@end

@implementation DataManager
static NSString * kFilePathCATS = @"CATS";
static NSString * kFilePathOFRS = @"OFRS";


- (instancetype)init {
    
    if([super init]) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(downloadData)
                                                 name:kNotificationNeedUpdateData
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(compareData:)
                                                 name:kNotificationDataWasParsed
                                               object:nil];
    
    _parser = [Parser new];
}

- (void)downloadData {
    if (self.parser == nil) {
        self.parser = [Parser new];
    }
    [self.parser start];
}


- (void)compareData:(NSNotification *)notification {
    
    NSArray <Category *>* downloadedCategories  = [notification.userInfo valueForKey:kCategories];
    NSArray <Offer *>*    downloadedOffers      = [notification.userInfo valueForKey:kOffers];
    
    
    
    if ([self isDifferentCachedAndDownloadedOffers:downloadedOffers]) {
        
        [[TMDiskCache sharedCache] setObject:downloadedOffers forKey:kFilePathOFRS];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationOffersWasUpdated
                                                            object:self
                                                          userInfo:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDownloadedOffersIsEqualToCached
                                                            object:self
                                                          userInfo:nil];
    }
    
    if ([self isDifferentCachedAndDownloadedCategories:downloadedCategories]) {
        
        [self downloadingImagesForCells:downloadedCategories];
        
        [[TMDiskCache sharedCache] setObject:downloadedCategories forKey:kFilePathCATS];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationCategoriesWasUpdated
                                                            object:self
                                                          userInfo:@{kCategories : downloadedCategories}];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDownloadedCategoriesIsEqualToCached
                                                            object:self
                                                          userInfo:nil];
    }
    
    
    
    [self deleteNotUsingImages:downloadedCategories];
}

#pragma mark - compare data

- (BOOL)isDifferentCachedAndDownloadedCategories:(nonnull NSArray <Category *>*)downloadedCategories {
    
    NSArray <Category *>* cachedCategories = [self getCategories];
    
    if (downloadedCategories.count == cachedCategories.count) {
        
        if ([[self allIDsFrom:cachedCategories] isEqualToString:[self allIDsFrom:downloadedCategories]]) {
            return NO;
        } else {
            return YES;
        }
    } else {
        return YES;
    }
}

- (BOOL)isDifferentCachedAndDownloadedOffers:(nonnull NSArray <Offer *>*)downloadedOffers {
    
    NSArray <Offer *>* cachedOffers = [self getOffers];
    
    if (downloadedOffers.count == cachedOffers.count) {
        
        if ([[self allIDsFrom:cachedOffers] isEqualToString:[self allIDsFrom:downloadedOffers]]) {
            
            return NO;
        } else {
            return YES;
        }
        
    } else {
        return YES;
    }}

- (nonnull NSString *)allIDsFrom:(NSArray *)array {
    
    static NSString *keyPath = @"identifier";
    
    NSMutableString *result = [NSMutableString string];
    
    for (id obj in array) {
        [result appendFormat:@"%d", [[obj valueForKeyPath:keyPath] integerValue]];
    }
    
    return result;
}

#pragma mark - get data

- (NSArray<Category *> *)getCategories {
    return (NSArray<Category *> *)[[TMDiskCache sharedCache] objectForKey:kFilePathCATS];
}

- (NSArray<Offer*> *)getOffers {
    return (NSArray<Offer *> *)[[TMDiskCache sharedCache] objectForKey:kFilePathOFRS];
}

- (nonnull NSArray <Offer *> *)getOffersWithCategoryID:(NSUInteger)ID {
    NSArray* data = [self getOffers];
    
    return [data filteredArrayUsingPredicate:
     [NSPredicate predicateWithFormat:@"(categoryId == %d)", ID]];
}



- (nonnull NSArray *)getCafeList {
    return [CafeList getCafeList];
}

- (void) downloadingImagesForCells:(NSArray <Category *>*)categories {
    
    for (Category* cat in categories) {
//        NSString* path = [NSHomeDirectory() stringByAppendingFormat:@"/%@.png", cat.name];
        NSString* fileName = [NSString stringWithFormat:@"%@.png", cat.name];
        
        if (![[[TMCache sharedCache] objectForKey:fileName] isKindOfClass:[UIImage class]]) {
            
            Offer* offer = [self getOffersWithCategoryID:cat.identifier].firstObject;
            
            [[SDWebImageManager sharedManager].imageDownloader downloadImageWithURL:offer.imageURL options:SDWebImageDownloaderUseNSURLCache progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                
            } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                [[TMCache sharedCache] setObject:image forKey:fileName];
            }];
        }
    }
}

- (void) deleteNotUsingImages:(NSArray <Category *>* )categories {
    NSMutableArray* oldCategories = [self categoriesNames];
    
    if (oldCategories != nil) {
        
        for (Category* cat in categories) {
            if (![oldCategories containsObject:cat.name]) {
                
                NSString* fileName = [cat.name stringByAppendingString:@".png"];
                [[TMCache sharedCache] removeObjectForKey:fileName];
            }
        }
    }
}
//сделал сравнение по именам потому что класс в массиве не видело и удаляло валидные картинки
- (NSMutableArray *)categoriesNames {
    
    NSArray* categories = [self getCategories];
    
    NSMutableArray* temp = [NSMutableArray array];
    
    for (Category *cat in categories) {
        [temp addObject:cat.name];
    }
    
    return temp;
    
}
#pragma mark - Cafe


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
