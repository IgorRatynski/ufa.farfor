//
//  DataManager.h
//  ufa.farfor
//
//  Created by Igor on 03.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Category;
@class Offer;
@class Cafe;

@interface DataManager : NSObject

- (void)downloadData;

- (nonnull NSArray <Category *>*)getCategories;
- (nonnull NSArray <Offer *> *)getOffersWithCategoryID:(NSUInteger)ID;
- (nonnull NSArray <Cafe *> *)getCafeList;
//- (void)saveData:(NSData *)dataArray;

@end
