
//
//  Parser.m
//  ufa.farfor
//
//  Created by Igor on 17.04.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//
#import <AFNetworking/AFNetworking.h>
#import "Ono.h"

#import "Parser.h"
#import "Category.h"
#import "Offer.h"


@interface Parser ()


@end

@implementation Parser

- (instancetype)init {
    if (self = [super init]) {
        [self start];
    }
    
    return self;
}

- (void)downloadData
{
    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"file" withExtension:@"xml"];
    
    AFHTTPSessionManager *downloader = [AFHTTPSessionManager manager];
    
    downloader.responseSerializer = [AFHTTPResponseSerializer serializer];
    [downloader.responseSerializer.acceptableContentTypes setByAddingObject:@"text/xml"];
    
    [downloader GET:fileURL.absoluteString
          parameters:nil
            progress:^(NSProgress * _Nonnull uploadProgress) {
         }
             success:^(NSURLSessionDataTask *task, NSData * responseObject) {
//                [self imitationOfWaitForDownload];
                [self parse:responseObject];
         }
             failure:^(NSURLSessionDataTask *  task, NSError *  error) {
                 
                 if ([error.domain isEqualToString:NSURLErrorDomain] || (error.code == NSURLErrorNotConnectedToInternet)) {
                     
                     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNotConnectedToInternet object:nil];
                 }
         }];
}

- (void)start {
    [self downloadData];
}


#pragma mark - parse elements

- (void)parse:(nonnull NSData *)data {
    
    ONOXMLDocument *XMLdoc = [ONOXMLDocument XMLDocumentWithData:data error:nil];
    
    NSArray * categories  = [self categoriesFromXMLdoc:XMLdoc];
    NSArray * offers      = [self     offersFromXMLdoc:XMLdoc];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDataWasParsed
                                                        object:self
                                                      userInfo:@{kCategories : categories,
                                                                 kOffers     : offers}];
    
}

- (nonnull NSArray<Category*>*)categoriesFromXMLdoc:(nonnull ONOXMLDocument *)XMLdoc {
    
    NSMutableArray<Category *> *categories = [NSMutableArray new];
    
    [XMLdoc.rootElement enumerateElementsWithXPath:@"//category" usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
        
        CategoryData categoryData;
        
        categoryData.identifier = [element[@"id"] integerValue];
        categoryData.name = [element stringValue];
        
        Category *category = [[Category alloc] initWithCategoryData:categoryData];
        
        [categories addObject:category];
    }];
    return categories;
}

- (nonnull NSArray<Offer*>*)offersFromXMLdoc:(nonnull ONOXMLDocument *)XMLdoc {

    NSMutableArray<Offer *> *offers = [NSMutableArray new];
    
    [XMLdoc.rootElement enumerateElementsWithXPath:@"//offer" usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {

        Offer *offer = [Offer new];
        
        offer.identifier = [element[@"id"] integerValue];
        offer.categoryId = [[[element firstChildWithTag:@"categoryId"] numberValue] integerValue];
        
        offer.price    = [[[element firstChildWithTag:@"price"] stringValue] doubleValue];
        
        offer.name     =  [[element firstChildWithTag:@"name"]  stringValue];
        offer.descript =  [[element firstChildWithTag:@"description"] stringValue];
        
        offer.shopURL  = [NSURL URLWithString:[[element firstChildWithTag:@"url"]     stringValue]];
        offer.imageURL = [NSURL URLWithString:[[element firstChildWithTag:@"picture"] stringValue]];
        
        NSMutableDictionary *params = [NSMutableDictionary new];
        
        [element enumerateElementsWithXPath:@"./param" usingBlock:^(ONOXMLElement *element, NSUInteger idx, BOOL *stop) {
            params[element[@"name"]] = [element stringValue];
        }];
        
        offer.params = params;
        
        
        [offers addObject:offer];
    }];
    
    return offers;
}

- (void)imitationOfWaitForDownload {
        
        usleep(100000);
    
}



@end
