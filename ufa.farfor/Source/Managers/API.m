//
//  API.m
//  ufa.farfor
//
//  Created by Igor on 02.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//
#import <UIKit/UIImage.h>

#import "API.h"
#import "AFNetworking.h"
#import "DataManager.h"
#import "Parser.h"
#import "Category.h"
#import "Offer.h"
#import "Cafe.h"

@interface API ()

@property (nonatomic, strong) DataManager        * dataManager;

@end

@implementation API

#pragma mark - init

+ (API *)sharedAPI {
    static dispatch_once_t once;
    static id instance;
    dispatch_once(&once, ^{
        instance = [self new];
    });
    return instance;
}

- (instancetype)init {
    self = [super init];
    if (self)
    {
        _dataManager  = [DataManager new];
    }
    return self;
}



#pragma mark - meth

- (void)downloadData {
    return [_dataManager downloadData];
}
- (NSArray<Cafe *> *)getCafeList {
    return [_dataManager getCafeList];
}
- (nonnull NSArray<Category *> *)getCategories {
    return [_dataManager getCategories];
}
- (nonnull NSArray<Offer *> *)getOffersWithCategoryID:(NSUInteger)ID {
    return [_dataManager getOffersWithCategoryID:ID];
}

#pragma mark - dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
