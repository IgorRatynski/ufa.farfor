//
//  MapAnnotation.h
//  ufa.farfor
//
//  Created by Igor on 03.12.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class Cafe;

@interface ClientMapAnnotation : NSObject <MKAnnotation>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString* phone;


+ (instancetype)initWithCafe:(Cafe*)cafe;

- (id)initWithCafe:(Cafe*)cafe;
- (void)setSubtitle:(NSString *)subtitle;
@end
