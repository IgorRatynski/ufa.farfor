//
//  UIImage+Resizing.h
//  ufa.farfor
//
//  Created by Igor on 28.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (Resize)

- (UIImage*)scaleToSize:(CGSize)size;
- (UIImage *)imageWithTintColor:(UIColor *)tintColor;

@end
