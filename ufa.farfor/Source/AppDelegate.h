//
//  AppDelegate.h
//
//
//  Created by Igor Ratynski on 12.11.15.
//  Copyright (c) 2015 Igor Ratynski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)showActivityIndicator:(NSString *)withTitleString currentView:(UIView *)currentView;
- (void)hideActivityIndicator;

@end

