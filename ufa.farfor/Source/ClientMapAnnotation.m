//
//  MapAnnotation.m
//  ufa.farfor
//
//  Created by Igor on 03.12.15.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#import <MapKit/MKAnnotationView.h>
#import "ClientMapAnnotation.h"
#import "Cafe.h"

#define size CGSizeMake(20, 20)


@implementation ClientMapAnnotation

+ (instancetype)initWithCafe:(Cafe*)cafe
{
    ClientMapAnnotation* annotation = [ClientMapAnnotation new];
    if (annotation) {
        
        [annotation setTitle:cafe.name];
        [annotation setSubtitle:cafe.address];
        [annotation setCoordinate:cafe.coordinate];
        [annotation setPhone:cafe.phone];
    }
    return annotation;

}
- (id)initWithCafe:(Cafe*)cafe
{
    self = [super init];
    if (self) {
        
//        [self setCafe:cafe];
        [self setTitle:cafe.name];
        [self setSubtitle:cafe.address];
        [self setCoordinate:cafe.coordinate];
    }
    return self;
}


@end
