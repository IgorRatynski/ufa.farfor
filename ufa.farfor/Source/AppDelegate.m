//
//  AppDelegate.m
//
//
//  Created by Igor Ratynski on 12.11.15.
//  Copyright (c) 2015 Igor Ratynski. All rights reserved.
//

#import "AppDelegate.h"
#import "API.h"
#import "MBProgressHUD.h"

@interface AppDelegate ()

@property (nonatomic, assign) BOOL isIndicatorStarted;
@property (nonatomic, strong) MBProgressHUD * progressHUD;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [API sharedAPI];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -

- (void)showActivityIndicator:(NSString *)withTitleString currentView:(UIView *)currentView {
    
    if (!_isIndicatorStarted) {
        
        _progressHUD = [MBProgressHUD showHUDAddedTo:currentView animated:NO];
        _progressHUD.mode = MBProgressHUDModeIndeterminate;
        _progressHUD.label.text = withTitleString;
        [UIView animateWithDuration:0.5f animations:^{
            _progressHUD.alpha = 0.f;
            _progressHUD.backgroundColor = [UIColor whiteColor];
            [_progressHUD showAnimated:NO];
        } completion:^(BOOL finished) {
            _progressHUD.alpha = 1.f;
            
        }];
    }
}

- (void)hideActivityIndicator {
    
    [UIView animateWithDuration:0.5f animations:^{
        _progressHUD.alpha = 1.f;
    } completion:^(BOOL finished) {
        _progressHUD.alpha = 0.f;
        [_progressHUD hideAnimated:NO];
        _isIndicatorStarted = FALSE;
    }];
    
}

@end
