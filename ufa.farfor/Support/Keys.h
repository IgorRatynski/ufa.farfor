//
//  Keys.h
//  ufa.farfor
//
//  Created by Igor on 06.09.16.
//  Copyright © 2016 Igor Ratynski. All rights reserved.
//

#ifndef Keys_h
#define Keys_h

#define kNotificationNeedUpdateData                         @"kNotificationNeedUpdateData"
#define kNotificationDataWasParsed                          @"kNotificationDataWasParsed"
#define kNotificationCategoriesWasUpdated                   @"kNotificationCategoriesWasUpdated"
#define kNotificationOffersWasUpdated                       @"kNotificationOffersWasUpdated"
#define kNotificationDownloadedCategoriesIsEqualToCached    @"kNotificationDownloadedCategoriesIsEqualToCached"
#define kNotificationDownloadedOffersIsEqualToCached        @"kNotificationDownloadedOffersIsEqualToCached"
#define kNotificationShowActivityIndicator                  @"kNotificationShowActivityIndicator"
#define kNotificationHideActivityIndicator                  @"kNotificationHideActivityIndicator"
#define kNotificationNotConnectedToInternet                 @"kNotificationNotConnectedToInternet"

#define kCategories             @"kCategories"
#define kOffers                 @"kOffers"
#endif /* Keys_h */
